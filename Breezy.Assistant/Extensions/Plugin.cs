﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.IO;
using NLog;

namespace Breezy.Assistant.Extensions
{
    public class PluginLoadEventArgs : EventArgs
    {
        public readonly Plugin Plugin;

        public PluginLoadEventArgs(Plugin plugin)
        {
            Plugin = plugin;
        }
    }

    public class Plugin
    {
        private readonly Dictionary<Type, IEnumerable<Type>> FProvides;

        public Assembly Assembly { get; protected set; }
        public bool IsSystem { get; protected set; }

        public IEnumerable<Type> this[Type index]
        {
            get
            {
                return Assembly.ExportedTypes.Where(x => x.IsSubclassOf(index));
            }
        }

        public IReadOnlyDictionary<Type, IEnumerable<Type>> Provides
        {
            get { return FProvides; }
        }

        public string Name
        {
            get { return Assembly.GetName().Name; }
        }

        public string Filename
        {
            get { return Assembly.Location; }
        }

        public IList<ConfigurableAttribute> Configurables { get; private set; }

        private Plugin(Assembly assembly, bool loadedManually)
        {
            Assembly = assembly;
            FProvides = new Dictionary<Type, IEnumerable<Type>>();
            Configurables = new List<ConfigurableAttribute>();
            IsSystem = !loadedManually;
        }

        public override string ToString()
        {
            return Name;
        }

        // Static members

        public static List<Plugin> PluginList { get; private set; } = new List<Plugin>();
        public static Dictionary<Type, List<Type>> ProvidedTypes { get; private set; } = new Dictionary<Type, List<Type>>();

        public static event EventHandler<PluginLoadEventArgs> PluginLoaded;

        private readonly static Dictionary<Type, Plugin> TypeDictionary = new Dictionary<Type, Plugin>();
        private static List<Type> Extensibles;

        static ILogger Logger = LogManager.GetCurrentClassLogger();

        private static void LoadAssemblyWithReferences(List<Assembly> loaded, Assembly asm)
        {
            Logger.Debug("Loading assembly {0}", asm);
            loaded.Add(asm);
            foreach (var a in asm.GetReferencedAssemblies().Select(x => Assembly.Load(x)).Where(x => !loaded.Contains(x)))
                LoadAssemblyWithReferences(loaded, a);
        }

        public static void Initialize(Assembly rootAssembly)
        {
            if (Extensibles == null)
                Extensibles = new List<Type>();

            var asmList = new List<Assembly>();
            LoadAssemblyWithReferences(asmList, rootAssembly);
            asmList.ForEach(z => Extensibles.AddRange(z.ExportedTypes.Where(x => x.GetCustomAttribute<ExtendableAttribute>() != null && !Extensibles.Contains(x))));
            asmList.ForEach(x => Register(x, false));
        }

        public static Plugin Register(string filename, bool manual = true)
        {
            return Register(Assembly.LoadFile(new FileInfo(filename).FullName), manual);
        }

        public static Plugin Register(Assembly assembly, bool manual = true)
        {
            if (Extensibles == null)
                Initialize(Assembly.GetCallingAssembly());

            Logger.Debug("Registering assembly {0}", assembly);

            var plugin = new Plugin(assembly, manual);
            foreach (var ext in Extensibles)
            {
                var list = plugin.Assembly.ExportedTypes.Where(x => x.IsSubclassOf(ext) || (ext.IsInterface && x.GetInterface(ext.Name) != null)).ToList();
                foreach (var type in list)
                {
                    if (!TypeDictionary.ContainsKey(type))
                        TypeDictionary.Add(type, plugin);
                }

                if (!plugin.FProvides.ContainsKey(ext))
                    plugin.FProvides.Add(ext, list);

                if (!ProvidedTypes.ContainsKey(ext))
                    ProvidedTypes.Add(ext, new List<Type>());

                ProvidedTypes[ext].AddRange(list);
            }

            var conf = plugin.Assembly.ExportedTypes
                .Where(x => x.CustomAttributes
                    .Any(z => z.AttributeType.Equals(typeof(ConfigurableAttribute))));

            foreach (var configurableType in conf)
            {
                Logger.Debug("Discovered configurable type {0}", configurableType);
                var methods = configurableType.GetMethods();
                var attr = configurableType.GetCustomAttribute<ConfigurableAttribute>();
                attr.Load = (ConfigureHandler)methods.Where(x => x.CustomAttributes.Any(z => z.AttributeType.Equals(typeof(LoaderAttribute)))).First().CreateDelegate(typeof(ConfigureHandler));
                attr.Save = (ConfigureHandler)methods.Where(x => x.CustomAttributes.Any(z => z.AttributeType.Equals(typeof(SaverAttribute)))).First().CreateDelegate(typeof(ConfigureHandler));
                plugin.Configurables.Add(attr);
            }

            PluginList.Add(plugin);

            PluginLoaded?.Invoke(null, new PluginLoadEventArgs(plugin));

            return plugin;
        }

        public static void Configure(IDictionary<string, IDictionary<string, object>> settings)
        {
            foreach (var p in PluginList)
            {
                foreach (var c in p.Configurables)
                {
                    var s = c.Section;
                    if (c.Load != null && settings.ContainsKey(s))
                    {
                        Logger.Debug("Configuring {0}", c.Section);
                        c.Load(settings[s]);
                    }
                }
            }
        }

        public static void SaveConfig(IDictionary<string, IDictionary<string, object>> settings)
        {
            foreach (var p in PluginList)
            {
                foreach (var c in p.Configurables)
                {
                    var s = c.Section;
                    if (!settings.ContainsKey(s))
                        settings.Add(s, new Dictionary<string, object>());
                    if (c.Save != null)
                    {
                        Logger.Debug("Saving configuration for {0}", c.GetType());
                        c.Save(settings[c.Section]);
                    }
                }
            }
        }

        public static Type GetType<T>(string typeName)
        {
            return ProvidedTypes[typeof(T)].First(x => x.FullName.Equals(typeName));
        }

        public static T CreateInstance<T>(params object[] args)
        {
            return (T)Activator.CreateInstance(typeof(T), args);
        }

        public static T CreateInstance<T>(Type type, params object[] args)
        {
            return (T)Activator.CreateInstance(type, args);
        }

        public static T CreateInstance<T>(string className, params object[] args)
        {
            return (T)Activator.CreateInstance(GetType<T>(className), args);
        }
    }
}


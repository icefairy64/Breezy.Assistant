﻿using System;
using System.Reflection;

namespace Breezy.Assistant.Extensions
{
    public static class Extension
    {
        public static string GetExtensionTitle(this Type obj)
        {
            return obj.GetCustomAttribute<NamedAttribute>()?.Name ?? obj.FullName;
        }
    }
}


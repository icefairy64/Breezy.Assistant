﻿using System;
using System.Collections.Generic;

namespace Breezy.Assistant.Extensions
{
    public delegate void ConfigureHandler(IDictionary<string, object> settings);

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class ExtendableAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class NamedAttribute : Attribute
    {
        public string Name { get; private set; }

        public NamedAttribute(string name)
        {
            Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class ConfigurableAttribute : Attribute
    {
        public ConfigureHandler Load { get; set; }
        public ConfigureHandler Save { get; set; }
        public string Section { get; set; }

        public ConfigurableAttribute(string section)
        {
            Section = section;
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class LoaderAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class SaverAttribute : Attribute
    {
    }
}


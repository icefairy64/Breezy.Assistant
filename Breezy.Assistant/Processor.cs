﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace Breezy.Assistant
{
    public static class Processor
    {
        public static IEnumerable<IEnumerable<string>> Process(Regex pattern, string text)
        {
            return pattern.Matches(text)
                .Cast<Match>()
                .Select(x => x.Groups
                    .Cast<Group>()
                    .Skip(1)
                    .Select(y => y.Value));
        }

        public static IEnumerable<IDictionary<string, string>> ProcessNamed(Regex pattern, string text)
        {
            return pattern.Matches(text)
                .Cast<Match>()
                .Select(x => pattern.GetGroupNames()
                    .ToDictionary(k => k, k => x.Groups[k].Value));
        }

        public static IEnumerable<IEnumerable<string>> Find(this string text, Regex pattern)
        {
            return Process(pattern, text);
        }

        public static IEnumerable<IDictionary<string, string>> FindNamed(this string text, Regex pattern)
        {
            return ProcessNamed(pattern, text);
        }
    }
}


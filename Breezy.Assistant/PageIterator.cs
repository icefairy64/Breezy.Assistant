﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;

namespace Breezy.Assistant
{
    public abstract class PageIterator<T> : IEnumerable<T>
    {
        private IEnumerable<T> Cache;

        public int Page { get; set; }

        public PageIterator()
        {
        }

        public abstract IEnumerable<T> Load(int page);
        public abstract Task<IEnumerable<T>> LoadAsync(int page);

        #region IEnumerable<T> implementation

        public IEnumerator<T> GetEnumerator()
        {
            while (Cache != null || ((Cache = Load(Page)) != null && Cache.Any()))
            {
                foreach (var img in Cache)
                    yield return img;
                Page++;
                Cache = null;
            }
        }

        #endregion

        #region IEnumerable implementation

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}


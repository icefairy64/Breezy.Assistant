﻿using System;
using System.Threading.Tasks;

namespace Breezy.Assistant
{
    public interface Loader<T, K>
    {
        Task<T> LoadAsync(K key);
    }
}


﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Breezy.Assistant
{
    public static class AsyncEnumerableExtensions
    {
        public static async Task<IEnumerable<R>> MapSeqAsync<T, R>(this IEnumerable<T> enumerable, Func<T, Task<R>> mapper)
        {
            var list = new List<R>();
            foreach (var item in enumerable)
                list.Add(await mapper(item));
            return list;
        }

        /// <summary>
        /// Performs sequential asyncronous mapping and aggregation
        /// </summary>
        public static async Task<TAccumulate> MapReduceAsync<T, TAccumulate>(this IEnumerable<T> underlying, Func<TAccumulate, TAccumulate, TAccumulate> combiner, Func<T, Task<TAccumulate>> mapper) where TAccumulate : class
        {
            TAccumulate accumulator = null;

            foreach (var item in underlying)
            {
                var res = await mapper(item);
                if (accumulator != null)
                    res = combiner(accumulator, res);
                else
                    accumulator = res;
            }

            return accumulator;
        }

        public static async Task<IEnumerable<T>> ChainAsync<T>(this IEnumerable<Task<T>> enumerable)
        {
            return await enumerable
                .Aggregate((a, x) => a.ContinueWith(async z => z.Result.Concat(await x)).Unwrap(), async x => (await x).ToEnumerable());
        }

        public static async Task<IEnumerable<R>> ChainMapAsync<T, R>(this IEnumerable<T> enumerable, Func<T, Task<R>> mapper)
        {
            return await enumerable
                .Aggregate((a, x) => a.ContinueWith(async z => z.Result.Concat(await x)).Unwrap(), async x => (await mapper(x)).ToEnumerable());
        }

        public static async Task<IEnumerable<T>> Chain<T>(this IEnumerable<Task<T>> enumerable)
        {
            var list = new List<T>();
            foreach (var item in enumerable)
                list.Add(await item);
            return list;
        }

        public static async Task<IEnumerable<T>> Paralellize<T>(this IEnumerable<Task<T>> enumerable, int degree)
        {
            var tasks = enumerable
                .Select((x, i) => new Tuple<int, Task<T>>(i, x))
                .GroupBy(x => x.Item1 % degree)
                .Select(x => x.Select(z => z.Item2).Chain());

            var res = await Task.WhenAll(tasks);

            IEnumerable<T> accumulator = null;

            foreach (var part in res)
            {
                if (accumulator != null)
                    accumulator = accumulator.Concat(part);
                else
                    accumulator = part;
            }

            return accumulator;
        }

        public static async Task<IEnumerable<R>> MapParAsync<T, R>(this IEnumerable<T> enumerable, Func<T, Task<R>> mapper, int degree)
        {
            var tasks = enumerable
                .Select((x, i) => new Tuple<int, T>(i, x))
                .GroupBy(x => x.Item1 % degree)
                .Select(x => x.Select(z => z.Item2).MapReduceAsync((a, z) => a.Concat(z), async z => (await mapper(z)).ToEnumerable()));

            return await Task.WhenAll(tasks)
                .ContinueWith(x => x.Result.Aggregate(new List<R>() as IEnumerable<R>, (a, z) => a.Concat(z)));
        }
    }
}


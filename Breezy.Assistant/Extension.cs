﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Breezy.Assistant
{
    public static class Extension
    {
        #region String

        /// <summary>
        /// Returns formatted string or default value if given string is null
        /// </summary>
        /// <returns>Formatted string or default value</returns>
        /// <param name="value">String value</param>
        /// <param name="format">Format string</param>
        /// <param name="defaultValue">Default value</param>
        public static string OrDefault(this string value, string format, string defaultValue)
            => value != null ? String.Format(format, value) : defaultValue;

        /// <summary>
        /// Returns substring of given string containing its right part with given length
        /// </summary>
        /// <param name="value">String</param>
        /// <param name="length">Substring length</param>
        public static string Right(this string value, int length)
            => value.Substring(value.Length - length, length);

        public static string SubstringUntil(this string value, char delimiter)
        {
            var index = value.IndexOf(delimiter);
            if (index < 0)
                return value;
            return value.Substring(0, index);
        }

        #endregion

        #region Object

        public static IEnumerable<T> ToEnumerable<T>(this T value)
        {
            return new SingleItemEnumerable<T>(value);
        }

        internal class SingleItemEnumerable<T> : IEnumerable<T>
        {
            private T Item;

            public SingleItemEnumerable(T item)
            {
                Item = item;
            }

            public IEnumerator<T> GetEnumerator()
            {
                yield return Item;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        #endregion

        #region IEnumerable

        public static StateSavingEnumerable<T> StateSaving<T>(this IEnumerable<T> enumerable)
        {
            return new StateSavingEnumerable<T>(enumerable);
        }

        public static IEnumerable<T> Each<T>(this IEnumerable<T> enumerable, int n)
        {
            return enumerable.Where((x, i) => i % n == 0);
        }

        public static TAccumulate Aggregate<T, TAccumulate>(this IEnumerable<T> enumerable, Func<TAccumulate, TAccumulate, TAccumulate> combiner, Func<T, TAccumulate> mapper)
        {
            var seed = mapper(enumerable.FirstOrDefault());
            return enumerable.Skip(1).Aggregate(seed, (a, x) => combiner(a, mapper(x)));
        }

        #endregion

        #region Stream

        public static byte[] ReadAll(this Stream stream, int bufferSize = 4096)
        {
            var buf = new byte[stream.Length];
            for (int i = 0; i < buf.Length; i += bufferSize)
            {
                var len = i + bufferSize < buf.Length ? bufferSize : bufferSize - (i + bufferSize - buf.Length);
                stream.Read(buf, i, len);
            }
            return buf;
        }

        public static void WriteAll(this Stream stream, byte[] buf, int bufferSize = 4096)
        {
            for (int i = 0; i < buf.Length; i += bufferSize)
            {
                var len = i + bufferSize < buf.Length ? bufferSize : bufferSize - (i + bufferSize - buf.Length);
                stream.Write(buf, i, len);
            }
        }

        public static string ReadAsString(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
                return reader.ReadToEnd();
        }

        #endregion

        #region IList

        public static List<T> AddRangeChainable<T>(this List<T> list, IEnumerable<T> enumerable)
        {
            list.AddRange(enumerable);
            return list;
        }

        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace Breezy.Assistant
{
    public class StateSavingEnumerable<T> : IEnumerable<T>
    {
        private IEnumerator<T> Back;

        public StateSavingEnumerable(IEnumerable<T> back)
        {
            Back = back.GetEnumerator();
        }

        public IEnumerable<T> TakeSlice(int count)
        {
            var list = new List<T>(count);
            while (Back.MoveNext() && count-- > 0)
                list.Add(Back.Current);
            return list;
        }

        public IEnumerator<T> GetEnumerator()
        {
            while (Back.MoveNext())
                yield return Back.Current;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}


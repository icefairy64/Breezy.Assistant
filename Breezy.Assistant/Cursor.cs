﻿using System;
using System.Threading.Tasks;

namespace Breezy.Assistant
{
    public class Cursor<T, K>
    {
        protected LinkIndex<K> CurrentIndex = LinkIndex<K>.Empty;
        protected Loader<Tuple<T, LinkIndex<K>>, K> Loader;

        public Cursor(Loader<Tuple<T, LinkIndex<K>>, K> loader)
        {
            Loader = loader;
        }

        public async Task<T> Next()
        {
            var pair = await Loader.LoadAsync(CurrentIndex.Next);
            CurrentIndex = pair.Item2;
            return pair.Item1;
        }

        public async Task<T> Prev()
        {
            var pair = await Loader.LoadAsync(CurrentIndex.Prev);
            CurrentIndex = pair.Item2;
            return pair.Item1;
        }
    }

    public struct LinkIndex<K>
    {
        public K Next;
        public K Current;
        public K Prev;

        public static LinkIndex<K> Empty
        {
            get { return new LinkIndex<K> { Next = default(K), Current = default(K), Prev = default(K) }; }
        }
    }
}


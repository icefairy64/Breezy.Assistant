﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Breezy.Assistant.Extensions;
using System.Reflection;

namespace Breezy.Assistant
{
    [Serializable]
    public class Settings
    {
        private static JsonSerializer Json = JsonSerializer.Create();

        private string Filename;

        [JsonProperty]
        public List<string> Plugins { get; set; }
        [JsonProperty]
        public Dictionary<string, IDictionary<string, object>> Config { get; set; }

        public Settings(string filename = null)
        {
            Plugins = new List<string>();
            Config = new Dictionary<string, IDictionary<string, object>>();
            Filename = filename;
            Json.Formatting = Formatting.Indented;
        }

        public void InitializePlugins(Assembly rootAssembly)
        {
            Plugin.Initialize(rootAssembly);

            foreach (var p in Plugins)
                Plugin.Register(p);

            Plugin.Configure(Config);
        }

        public void Save()
        {
            Plugin.SaveConfig(Config);
            using (var w = new JsonTextWriter(new StreamWriter(Filename)))
                Json.Serialize(w, this);
        }

        // Static members

        public static Settings Load(string filename)
        {
            using (var r = new JsonTextReader(new StreamReader(filename)))
            {
                var s = Json.Deserialize<Settings>(r);
                s.Filename = filename;
                return s;
            }
        }
    }
}


﻿namespace Breezy.Assistant.FSharp

open System.Text.RegularExpressions

module Regex = 

    let findMatches (regex : Regex) text = 
        regex.Matches (text)
            |> Seq.cast<Match>
            |> Seq.map (fun z -> z.Groups |> Seq.cast<Group> |> Seq.mapi (fun v idx -> (v, idx)) |> Seq.filter (fun (idx, v) -> idx > 0) |> Seq.map (fun (idx, g) -> g.Value |> List.ofSeq))

    let findMatchesNamed (regex : Regex) text = 
        let groups = 
            Seq.zip (regex.GetGroupNumbers ()) (regex.GetGroupNames ())
            |> Map.ofSeq

        regex.Matches (text)
            |> Seq.cast<Match>
            |> Seq.map (fun z -> z.Groups |> Seq.cast<Group> |> Seq.mapi (fun v idx -> (v, idx)) |> Seq.filter (fun (idx, v) -> idx > 0) |> Seq.map (fun (idx, g) -> (groups.Item idx, g.Value)) |> Map.ofSeq)
﻿namespace Breezy.Assistant.FSharp

open System.Collections.Generic
open Newtonsoft.Json.Linq

module Json =

    type JsonObject =
        | Object of Map<string, JsonObject>
        | Array of JsonObject list
        | Boolean of bool
        | Integer of int
        | Float of float
        | Text of string
        | None

    let rec dismember (token : JToken) = 
        let dismemberArray (array : JArray) =
            array |> Seq.map dismember |> List.ofSeq

        let dismemberObject (obj : JObject) =
            (obj :> IEnumerable<KeyValuePair<string, JToken>>) 
            |> Seq.map (fun (item : KeyValuePair<string, JToken>) -> (item.Key, dismember item.Value)) 
            |> Map.ofSeq

        match token.Type with
        | JTokenType.Boolean -> Boolean (token.ToObject<bool> ())
        | JTokenType.Integer -> Integer (token.ToObject<int> ())
        | JTokenType.Float   -> Float   (token.ToObject<float> ())
        | JTokenType.String  -> Text    (token.ToObject<string> ())
        | JTokenType.Array   -> Array   (dismemberArray (token.ToObject<JArray> ()))
        | JTokenType.Object  -> Object  (dismemberObject (token.ToObject<JObject> ()))
        | _                  -> None

    let parse data =
        JToken.Parse (data) |> dismember
